const { Socket } = require('net');
const { WEB_SOCKET_SERVER_PORT } = require('./config');

let wsHost = `http://localhost:${WEB_SOCKET_SERVER_PORT}`;

const socket = require('socket.io-client')(wsHost);
const socketStream = require('socket.io-stream');

socket.on('reconnecting', (attempt) => {
  console.log(`WebSocket client reconnect attempt ${attempt}`);
});

socket.on('connect', function () {
  console.log(`WebSocket client ${socket.id} connected to ${wsHost}`);
});

socket.on('disconnect', function () {
  console.log(`WebSocket client disconnected`);
});

socketStream(socket).on('proxy-stream', (stream, data) => {
  let { port, host, head, httpVersion } = data;
  console.log(`New stream to ${host}:${port} from proxy server`);
  let proxySocket = new Socket();

  proxySocket.on('error', err => {
    console.error(err);
  });

  proxySocket.connect(port, host, function () {
    proxySocket.write(head);
    stream.write(`HTTP/${httpVersion} 200 Connection established\r\n\r\n`);
    stream.pipe(proxySocket);
    proxySocket.pipe(stream);
  });
});
