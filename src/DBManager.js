const escapeStringRegexp = require('escape-string-regexp');
const { mongoose } = require('./Models/db');
const { SOCKET_HISTORY_SIZE } = require('../config');

class DBManager {
  constructor(url = 'mongodb://localhost/dnsflex') {
    this.connection = mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })
      .then(conn => {
        this.Users = conn.model('User');
        console.log('Connected to mongodb on', url);
        return conn;
      })
      .catch(err => {
        console.error('Cant connect to mongodb on', url);
        console.error(err);
      })
  }

  getNewSocket({ country, region, usage_type, isp}, exclSocketIds = []) {
    const priorityArr = [];
    if (region) {
      priorityArr.push({$cond: [{$eq: ['$region', region]}, 4, 0]});
    }
    if (usage_type) {
      priorityArr.push({$cond: [{$eq: ['$usageType', usage_type]}, 2, 0]});
    }
    if (isp) {
      priorityArr.push({$cond: [{$regexMatch: {input: '$isp', regex: new RegExp('^'+ escapeStringRegexp(isp))}}, 1, 0]});
    }
    if (priorityArr.length === 0) {
      priorityArr.push(0);
    }
    return this.Users.aggregate()
        .match({ country })
        // priority is sum of weighs: region match +4, type match +2, isp match +1
        .project({ socketId: 1, connections: 1, priority: { $add: priorityArr }})
        .sort({ priority: 'desc', connections: 'asc' })
        .group({ _id : "$priority", items: { $push: "$$ROOT" } })
        .limit(SOCKET_HISTORY_SIZE + 1)
        .exec().then(socketGroups => {
          if (socketGroups.length <= 0) {
            throw new Error(`Cant find socket for ${country}`);
          }
          // pick last one element on array - it has the most priority
          const sockets = socketGroups.sort((f,n) => f._id - n._id)[socketGroups.length - 1].items;
          const excluded = new Set(exclSocketIds);
          const newSockets = sockets.filter(socket => !excluded.has(socket.socketId));
          if (newSockets.length > 0) {
            return newSockets[0].socketId;
          } else {
            // all sockets in exclusion list, finding least recently used socket
            for (const socketId of exclSocketIds) {
              if (sockets.find(socket => socket.socketId === socketId)) {
                return socketId;
              }
            }
          }
         });
  }

  async incrementConnectionsFor(socketId = '') {
    return await this.Users.updateOne({ socketId }, { $inc: { connections: 1 } });
  }

  async decrementConnectionsFor(socketId = '') {
    return this.Users.updateOne({ socketId }, { $inc: { connections: -1 } });
  }

  saveActiveAppUser({ socketId, location, ip }) {
    return this.Users.create({
      socketId, ...location, ip, createdDate: new Date(), connections: 0
    }).catch(err => {
      console.log(`Cant store socket ${socketId}`);
      console.error(err)
    });
  }

  async deleteInactiveUserBySocketId(inactiveSocketId) {
    return await this.Users.deleteOne({ socketId: inactiveSocketId });
  }

  deleteAllUsers() {
    return this.Users.deleteMany({})
      .catch(err => {
        console.error('Error deleting all users', err)
      })
  }

  getAll() {
    return this.Users.find({}).sort({ 'createdDate': 1, 'connections': 1 }).lean()
  }

}

module.exports = DBManager;
