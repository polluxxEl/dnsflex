const jsoncsv = require('json-csv')
const { IP2Location_get_all } = require('./ip2Loc');
const { INSTANCE_PROXY_CLIENT_HOST } = require('../config');

const regex_hostport = /^([^:]+)(:([0-9]+))?$/;


const getAppUserLocationByIp = (ip) => {
  if (ip === '127.0.0.1' && INSTANCE_PROXY_CLIENT_HOST) {
    ip = INSTANCE_PROXY_CLIENT_HOST
  }
  return IP2Location_get_all(ip)
  //return Promise.resolve({country_short: 'ua', country_long: 'Ukraine', city: 'Lviv', region: 'Lviv', usage_type: 'mob', isp: 'volya'})
    .then(result => {
      const { country_short, country_long, city, usage_type, region, netspeed, isp } = result;

      return ({
        country: (country_short || '').toLowerCase(),
        region: (region || '').toLowerCase(),
        city,
        countryLong: country_long,
        usageType: (usage_type || '').toLowerCase(),
        netSpeed: netspeed,
        isp: (isp || '').toLowerCase()
      });
    })
};

const passwordSchema = 'usage_type-country-region-interval-threads-bandwidth-isp';

const parsePassword = (password = '') => {
  const parts = password.split('-');
  return {
    usage_type: (parts[0] || '').toLowerCase(),
    country: (parts[1] || '').toLowerCase(),
    region: parts[2] || '',
    interval: parts[3] || 9999,
    threads: parseInt(parts[4]) || 0,
    bandwidth: parseInt(parts[5]) || 0,
    isp: (parts[6] || '').toLowerCase(),
  };
};

const getHostPortFromString = (hostString, defaultPort) => {
  let host = hostString;
  let port = defaultPort;

  let result = regex_hostport.exec(hostString);
  if (result != null) {
    host = result[1];
    if (result[2] != null) {
      port = result[3];
    }
  }
  return ([host, port]);
};

const getReportHtml = (clients) => {
  let header = `<th>Country</th>
  <th>Region</th>
  <th>City</th>
  <th>Usage Type</th>
  <th>Public IP</th>
  <th>Uptime</th>
  <th>UsedInSessions</th>
  <th>ISP</th>
  <th>SocketId</th>`;

  const content = clients.reduce((acc, item) => {
    const { country, region, city, countryLong, ip, createdDate, usageType, connections, isp, socketId,
      // Just to remove those properties from report
      row, _id, __v,
      ...rest } = item;

    let cDate = new Date(createdDate);
    let diff = Date.now() - cDate.valueOf();// in milliseconds
    let diffSeconds = diff / 1000;
    let uptime = diffSeconds + ' s';
    if (diffSeconds > 60) {
      let seconds = (diffSeconds % 60).toFixed(2);
      uptime = Math.floor(diffSeconds / 60) + ' m ' + seconds + ' s';
    } else if (diffSeconds > 3600) {
      let minutes = ((diffSeconds % 3600) / 60).toFixed();
      uptime = Math.floor(diffSeconds / 3600) + ' h ' + minutes + ' m'
    }
    acc += `
  <tr>
  <td>${country}</td>
  <td>${region}</td>
  <td>${city}</td>
  <td>${usageType}</td>
  <td>${ip}</td>
  <td>${uptime}</td>
  <td>${connections}</td>
  <td>${isp}</td>
  <td>${socketId}</td>
  </tr>
  `;
  //   ${Object.keys(rest).map(key => {
  // const headerHtml = `<th>${key}</th>`;
  //   if (header.indexOf(headerHtml) === -1) {
  //     header += headerHtml;
  //   }
  //   return `<td>${rest[key]}</td>`
  // })}
    return acc;
  }, '');

  return `
  <table>
    <tr>
    ${header}
    </tr>
    ${content}
  </table>
`;
};

const formatReportObject = (clients) => {
  return clients.map(({ ip, countryLong, region, createdDate, netspeed, usageType, city, connections, isp, socketId, row, _id, __v, ...rest }) => {
    let cDate = new Date(createdDate);
    let diff = Date.now() - cDate.valueOf();// in milliseconds
    let diffSeconds = Math.floor(diff / 1000);
    let uptime = diffSeconds + ' s';
    if (diffSeconds > 60) {
      let seconds = (diffSeconds % 60).toFixed(2);
      uptime = Math.floor(diffSeconds / 60) + ' m ' + seconds + ' s';
    } else if (diffSeconds > 3600) {
      let minutes = ((diffSeconds % 3600) / 60).toFixed();
      uptime = Math.floor(diffSeconds / 3600) + ' h ' + minutes + ' m'
    }
    return {
      country: countryLong,
      region,
      city,
      usageType,
      ip,
      uptime,
      connections,
      isp,
      socketId
    }
  });
};

const getReportJson = (clients) => JSON.stringify(formatReportObject(clients), '\n', 2);

const getReportCsv = (clients) => jsoncsv.buffered(formatReportObject(clients), {
  fields: [
    { name: 'country' },
    { name: 'region' },
    { name: 'city' },
    { name: 'usageType' },
    { name: 'ip' },
    { name: 'uptime' },
    { name: 'connections' },
    { name: 'isp' },
    { name: 'socketId' },
  ]
});

module.exports = {
  passwordSchema,
  getAppUserLocationByIp,
  parsePassword,
  getHostPortFromString,
  getReportHtml,
  getReportJson,
  getReportCsv,
};
