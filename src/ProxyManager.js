const util = require('./util');
const { SOCKET_HISTORY_SIZE } = require('../config');

/**
 * Retrieving interval in ms (incoming value in minutes)
 *
 * @param interval minutes
 * @returns {number} milliseconds
 */
function minutesToMs(interval = '0') {
  return parseInt(interval) * 60000;
}

/**
 * Class maintains sessionId-socket links, responsible for IP rotation and socket selection for first request or when
 * session criteria changed.
 *
 * Uses SocketIoManager to get real Socket object by socketId and register socket disconnect listener to
 * discard all sessions which use this socket
 */
class ProxyManager {
  constructor(dbManager, ioManager) {
    this.db = dbManager;
    this.io = ioManager;

    /**
     * Maps sessionId (username from HTTP proxy request) to socketId
     * with extra data to invalidate mapping in case of HTTP Proxy request params (password) change
     *   country, region, type, isp - for matching sockets
     *   intervalMs, assignTimeMs - for rotating sockets
     *   prevSockets [] - array of previous sockets - FIFO of size SOCKET_HISTORY_SIZE
     */
    this.connections = {};

    this.io.onSocketDisconnect((socket) => {
      this.handleWebSocketDisconnect(socket.id)
    })
  }

  connectionsEqual(a, b) {
    return (
      a.country === b.country &&
      a.usage_type === b.usage_type &&
      a.region === b.region &&
      a.type === b.type &&
      a.isp === b.isp &&
      a.intervalMs === b.intervalMs
    );
  }

  removeSession(sessionId) {
    const socketId = this.connections[sessionId].socketId;
    if (socketId) {
      this.db.decrementConnectionsFor(socketId);
    }
    delete this.connections[sessionId];
  }

  initSession(sessionId, connOpts) {
    this.connections[sessionId] = connOpts;
    this.connections[sessionId].assignTimeMs = null;
    this.connections[sessionId].lastRequestTime = null;
    this.connections[sessionId].socketId = null;
    this.connections[sessionId].prevSockets = [];
  }

  invalidateSessionSocket(sessionId) {
    const conn = this.connections[sessionId];
    if (conn.socketId !== null) {
      /**
       * Remove socket from prevSockets in case it is already there (we are low on matching sockets,
       * so using sockets from prevSockets)
       */
      conn.prevSockets = conn.prevSockets.filter(socketId => socketId !== conn.socketId);
      conn.prevSockets.push(conn.socketId);
      if (conn.prevSockets.length > SOCKET_HISTORY_SIZE) {
        conn.prevSockets.shift();
      }
      this.db.decrementConnectionsFor(conn.socketId);
      conn.socketId = null;
    }
  }

  needsRotation(connOpts) {
    const now = Date.now();
    const elapsedTime = now - connOpts.assignTimeMs;
    return connOpts.socketId && elapsedTime > connOpts.intervalMs;
  }

  async getSocket(sessionId, password) {
    const { country, region, usage_type, isp, interval } = util.parsePassword(password);
    const intervalMs = minutesToMs(interval);
    const newConnOpts = {
      country,
      region,
      usage_type,
      isp,
      intervalMs,
    }
    console.log(`Select socket for ${sessionId}: ${usage_type} - ${country} - ${region} - ${interval} - ${isp}`);

    if (!this.connections[sessionId]) {
      /**
       * Init new session
       */
      this.initSession(sessionId, newConnOpts);
    } else if (this.connections[sessionId] && !this.connectionsEqual(this.connections[sessionId], newConnOpts)) {
      /**
       * Session options updated, discarding previous settings, rotation timer and sockets history
       */
      this.removeSession(sessionId);
      this.initSession(sessionId, newConnOpts);
    }

    /**
     * Rotate socket - remove socketId to select new one later
     */
    if (this.connections[sessionId] && this.needsRotation(this.connections[sessionId])) {
      this.invalidateSessionSocket(sessionId);
    }

    /**
     * Select first socket for new session or new socket for old session after rotation
     */
    if (this.connections[sessionId] && !this.connections[sessionId].socketId) {
      await this.selectNewSocket(sessionId);
    }

    /**
     * socketId should be present at this stage, otherwise we have no socket for connection
     */
    if (this.connections[sessionId].socketId) {
      this.connections[sessionId].lastRequestTime = Date.now();
      return this.io.getSocket(this.connections[sessionId].socketId);
    } else {
      console.log(`No socket for ${sessionId}`);
      // no socket
    }
  }

  async selectNewSocket(sessionId) {
    const conn = this.connections[sessionId];
    const socketFilter = (
        ({country, region, usage_type, isp}) => ({country, region, usage_type, isp})
    )(conn);
    conn.socketId = await this.db.getNewSocket(socketFilter, conn.prevSockets);
    this.db.incrementConnectionsFor(conn.socketId);
    conn.assignTimeMs = Date.now();
  }

  handleWebSocketDisconnect(disconnectedSocketId) {
    // TODO: add map from socketId => sessionId to improve disconnect performance
    Object.values(this.connections).forEach(({ socketId, sessionId }) => {
      if (disconnectedSocketId === socketId) {
        delete this.connections[sessionId];
      }
    });
  }
}

module.exports = ProxyManager;
