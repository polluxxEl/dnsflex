const http = require('http');
const socketIO = require('socket.io');

/**
 * SocketIoManager initiates socket.io server, listens for incoming websocket connections,
 * store them internally and able to return socket by socketId (even though socket.io has
 * this natively io.sockets.connected[socketId])
 * Also registers connect/disconnect listeners to integrate with other components
 *
 * Should be called socketRegistry?
 */

class SocketIoManager {
  constructor(port) {
    if (!port) throw new Error('Port should be provided');

    this.port = port;
    this.socketDisconnectListeners = [];
    this.socketConnectListeners = [];
    this.socketsStorage = {};

    this.DEFAULT_SOCKET_EVENTS = {
      CONNECTION: 'connection',
      ERROR: 'error',
      DISCONNECT: 'disconnect',
      CONNECT: 'connect'
    }
  }

  getSocket(id) {
    return this.socketsStorage[id];
  }

  getRandomSocket() {
    const socketIds = Object.keys(this.socketsStorage);
    return this.socketsStorage[socketIds[Math.floor(Math.random()*socketIds.length)]];
  }

  startSocketServer() {
    this.httpSocketServer = http.createServer();
    this.io = socketIO(this.httpSocketServer);

    this.io.on(this.DEFAULT_SOCKET_EVENTS.CONNECTION, socket => {
      this.socketsStorage[socket.id] = socket;
      console.log('WebSocket connected:', socket.id);

      this.socketConnectListeners.forEach(listener => {
        listener(socket);
      });

      socket.on(this.DEFAULT_SOCKET_EVENTS.DISCONNECT, () => {
        console.log('WebSocket disconnected:', socket.id);
        delete this.socketsStorage[socket.id];
        this.socketDisconnectListeners.forEach(listener => {
          if (typeof listener === 'function') {
            listener(socket);
          }
        });
      });

      socket.on(this.DEFAULT_SOCKET_EVENTS.ERROR, (err) => {
        console.error('WebSocket error:', socket.id);
      })
    });

    this.httpSocketServer.listen(this.port, () => {
      console.log(`Listening for socket.io connections on`, this.port);
    })

  }

  onSocketDisconnect(listener) {
    this.socketDisconnectListeners.push(listener)
  }

  onSocketConnect(listener) {
    this.socketConnectListeners.push(listener);
  }

}

module.exports = SocketIoManager;
