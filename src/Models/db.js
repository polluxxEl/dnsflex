const mongoose = require('mongoose');

mongoose.set('debug', true);

const Db = new mongoose.Schema({
  name: 'string',
  socketId: 'string',
  connections: 'number',
  sessionId: 'string',
  createdDate: 'date',
  // Location:
  city: 'string',
  country: 'string',
  countryLong: 'string',
  region: 'string',
  //iptype: 'string',
  ip: 'string',
  netSpeed: 'string',
  usageType: 'string',
  isp: 'string'
});

module.exports = {
  Users: mongoose.model('User', Db),
  mongoose,
};
