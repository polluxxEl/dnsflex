const ThrottleGroup = require('stream-throttle').ThrottleGroup;

class ProxyThrottler {
  constructor() {
    this.sessionConnections = {};
    this.sessionThrottlers = {};
  }

  /**
   *
   * @param sessionId
   * @param rateMBps - MBps, 0 if unlimited
   * @returns {null|ThrottleGroup}
   */
  getThrottleGroup(sessionId, rateMBps = 0) {
    if (rateMBps === 0) {
      return null;
    }

    const rate = rateMBps * 1024 * 1024;

    if (!this.sessionThrottlers[sessionId] || this.sessionThrottlers[sessionId].rate !== rate) {
      delete this.sessionThrottlers[sessionId];
      this.sessionThrottlers[sessionId] = {
        rate,
        throttler: new ThrottleGroup({ rate }),
      };
    }
    if (this.sessionThrottlers[sessionId] && this.sessionThrottlers[sessionId].rate === rate) {
      return this.sessionThrottlers[sessionId].throttler;
    }
  }

  incConnections(sessionId, maxConnections = 0) {
    if (maxConnections === 0) {
      return true;
    }
    if (!this.sessionConnections[sessionId] || this.sessionConnections[sessionId].maxConnections !== maxConnections) {
      delete this.sessionConnections[sessionId];
      this.sessionConnections[sessionId] = {
        maxConnections,
        connections: 0,
      }
    }
    if (this.sessionConnections[sessionId] && this.sessionConnections[sessionId].maxConnections === maxConnections) {
      if (this.sessionConnections[sessionId].connections < maxConnections) {
        this.sessionConnections[sessionId].connections += 1;
        return true;
      } else {
        return false;
      }
    }
  }

  decConnections(sessionId, maxConnections = 0) {
    if (maxConnections === 0) {
      return;
    }
    if (this.sessionConnections[sessionId]) {
      this.sessionConnections[sessionId].connections -= 1;
    }
  }
}

module.exports = ProxyThrottler;
