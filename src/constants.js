module.exports = {
  CONNECTION_TYPE: {
    STICKY: 'sticky',
    ROTATE: 'rotate',
    MOBILE: 'mobile',
  }
}
