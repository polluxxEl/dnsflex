const http = require('https');
const cacheManager = require('cache-manager');
const mongoStore = require('cache-manager-mongodb');
const { IP2_KEY, IP_CACHE_TTL, DB_CONNECTION } = require('../../config');

const cache = cacheManager.caching({
  store: mongoStore,
  uri: DB_CONNECTION,
  options : {
    collection : "cacheManager",
    compression : false,
    poolSize : 5,
    autoReconnect: true,
  },
  ttl: IP_CACHE_TTL,
});

const getIpLocation = function (ip) {
  return new Promise((resolve, reject) => {
    let data = '';
    const options = {
      hostname: 'api.ip2location.com',
      headers: {
        'Content-Type': 'application/json'
      },
      path: `/v2/?ip=${ip}&key=${IP2_KEY}&package=WS23`//&addon=country,region,city,geotargeting`
    };
    http.get(options, res => {
      res.on('data', (d) => {
        data += d;
      });

      res.on('error', err => reject(err))

      res.on('end', () => {
        const {
          country_code,
          country_name,
          region_name,
          mobile_brand,
          usage_type, // COM, ORG, GOV, MIL, EDU, LIB, CDN, ISP, MOB, DCH, SES, RSV
          net_speed, // DIAL, DSL, COMP
          isp,
          city = {},
          city_name,
          region = {},
        } = JSON.parse(data);

        return resolve({
          country_short: country_code,
          country_long: country_name,
          region: region && region.name ? region.name : region_name,
          city: city && city.name ? city.name : city_name,
          mobilebrand: mobile_brand,
          netspeed: net_speed,
          usage_type: usage_type ? usage_type.toLowerCase() : '',
          isp
        })
      })
    })
  })
};

module.exports = {
  IP2Location_get_all: async function (ip) {
    return cache.wrap(ip, function() {
      console.log(`No location info for ${ip} in cache`);
      return getIpLocation(ip);
    });
  }
};
