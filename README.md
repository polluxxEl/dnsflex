# DNSFlex_Proxy

Proxy server to tunnel requests through connected WebSockets

## Install Node.js:
  - `wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.2/install.sh | bash`
  - `export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"  && \. "$NVM_DIR/nvm.sh"`
  - `nvm install --lts`

## Install pm2: 
  - `npm install -g pm2`

## Install MongoDB:
  - `wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -`
  - `echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list`
  - `sudo apt-get update`
  - `sudo apt-get install -y mongodb-org`

## Setup MongoDB:
  Change mongo port:
  - `nano /etc/mongod.conf` set `port` to 27017

### Start MongoDB 
  - `sudo service mongod start`

### Create MongoDB database 
  - `mongo --port 27017`
  - then in mongoshell `use dnsflex`, quit from mongoshell terminal

## Clone repository
  - `git clone https://github.com/elifTech/DNSFlex_Proxy.git`

## Project setup

  - set IP2_KEY in the PROJECT_ROOT_FOLDER/config.js 

  - create file usernames.txt in PROJECT_ROOT_FOLDER/ .The file have to contain allowed usernames for proxy
  - `npm install` to install required modules

## To run proxy

  From project root folder:
  - `pm2 start VoilaProxyServer.js`

### To manipulate app state 
  - see pm2 docs  https://pm2.keymetrics.io/docs/usage/quick-start/

### Auto Start on Boot

# pm2 startup systemd
# pm2 list
# pm2 status VoilaProxyServer
# systemctl status pm2-root.service


# Socket selection and rotation

We use http proxy username as a session identifier and http proxy password for connection options.
Password structure is usage_type-country-region-interval-threads-bandwidth-isp.

If no sockets from requested country available proxy will respond with 404 Not found.
Region, usage_type and isp options also used to select exactly matching sockets, but if we have no socket corresponding
to all provided conditions, we can relax them one by one. (first isp is relaxed, then usage_type and finally region).
Socket with min connections selected from the list of matching sockets. 
There is a list of previous sockets for each session, to do not use them during rotation.
Number of sockets in the history configured in config.js file (Default is 5). If all matching sockets were already recently used,
proxy server will start to re-use them starting from least recently used.  
