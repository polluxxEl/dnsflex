process.title = 'TCP_PROXY_APP';

const net = require('net');
const config = require('../config');
const server = net.createServer();

server.on('connection', (clientToProxySocket) => {
  console.log('Client connected to proxy app');
  let appUserHost = config.INSTANCE_PROXY_CLIENT_HOST;
  let appUserPort = config.INSTANCE_PROXY_CLIENT_PORT;
  // We need only the data once, the starting packet
  let proxyToServerSocket = setUpConnectionFor(clientToProxySocket, appUserHost, appUserPort)

  clientToProxySocket.on('error', (err) => {
    console.log('SERVER_SOCKET_CLIENT ERROR');
    console.log(err);
  });
  proxyToServerSocket.on('error', (err) => {
    console.log('DESKTOP_SOCKET_USER ERROR');
    console.log(err);
    proxyToServerSocket = setUpConnectionFor(clientToProxySocket, 'localhost', appUserPort)
  });
});

function setUpConnectionFor(socket, host, port) {
  let serverToDesktopSocket = net.createConnection({ host, port }, () => {
    console.log('Connection to remote client established');
    // Piping the sockets
    serverToDesktopSocket.pipe(socket);
    socket.pipe(serverToDesktopSocket);

  });
  return serverToDesktopSocket
}

server.on('error', (err) => {
  console.log('SERVER ERROR');
  console.log(err);
});
server.on('close', () => {
  console.log('Client disconnected');
});
server.listen(config.PROXY_SERVER_PORT, () => {
  console.log('Proxy server running at http://localhost:' + config.PROXY_SERVER_PORT);
});