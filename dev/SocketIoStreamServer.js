process.title = 'DNSFlex Proxy Server';

const http = require('http');
const socketStream = require('socket.io-stream');

const SocketIoManager = require('../src/SocketIoManager');
const DBManager = require('../src/DBManager');
const util = require('../src/util');

const { PROXY_SERVER_PORT, WEB_SOCKET_SERVER_PORT, DB_CONNECTION } = require('../config');

const proxyServer = http.createServer();

const dbManager = new DBManager(DB_CONNECTION);
const ioManager = new SocketIoManager(WEB_SOCKET_SERVER_PORT);

ioManager.startSocketServer();

ioManager.onSocketDisconnect(socket => {
  // dbManager.deleteAppUser(socket.id);
  // checkActiveRequestsForSocket(socket.id)
});

ioManager.onSocketConnect(socket => {
  console.log(`\n---=== On socket connect listener: \n`)
  const { address } = socket.handshake;
  util.getAppUserLocationByIp(address.replace('::ffff:', ''))
  .then(location => dbManager.saveActiveAppUser(socket.id, location));
});

const hostPortRegex = /^([^:]+)(:([0-9]+))?$/;

const getHostPortFromString = function (hostString, defaultPort) {
  let host = hostString;
  let port = defaultPort;

  let result = hostPortRegex.exec(hostString);
  if (result != null) {
    host = result[1];
    if (result[2] != null) {
      port = result[3];
    }
  }

  return ([host, port]);
};

proxyServer.addListener('connect', (req, socket, head) => {
  let hostPort = getHostPortFromString(req.url, 443);
  let host = hostPort[0];
  let port = parseInt(hostPort[1]);

  console.log(`Proxying request for ${socket.address().address} to ${host}:${port}`);

  if (req.headers['proxy-authorization']) {
    let auth = req.headers['proxy-authorization'];
    let [user, pass] = Buffer.from(auth.split(" ")[1], 'base64').toString().split(':');

    console.log(`AUTH: `, `USER: ${user} and PASS: ${pass}`)
  }

  const appSocket = ioManager.getRandomSocket();
  if (!appSocket) {
    // no websocket available
    socket.write(`HTTP/${req.httpVersion} 500 Connection error\r\n\r\n`);
    socket.end();
    return
  }

  const stream = socketStream.createStream({});

  socketStream(appSocket).emit('proxy-stream', stream, {
    port, host, head: head.toString(), httpVersion: req.httpVersion
  });

  socket.pipe(stream);
  stream.pipe(socket);
});

proxyServer.listen(PROXY_SERVER_PORT, function () {
  console.log('APP listening on *:', PROXY_SERVER_PORT);
});

function exitHandler(code) {
  console.log(`Terminating app, exit code ${code}`)
  if (code == 200) {
    return
  }
  if (!dbManager) return

  return dbManager.deleteAllUsers()
    .then(ok => {
      process.exit(200)
    })
}


process.on('exit', exitHandler);
//catches ctrl+c event
// process.on('SIGINT', exitHandler);
// catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR1', exitHandler);
process.on('SIGUSR2', exitHandler);
//process.on('uncaughtException', exitHandler);
process.on('uncaughtException', (e) => {
  console.log(e);
});
