const http = require("http");
const net = require('net');
const { INSTANCE_PROXY_CLIENT_PORT } = require('../config');

const server = http.createServer().listen(
  INSTANCE_PROXY_CLIENT_PORT
);

const regex_hostport = /^([^:]+)(:([0-9]+))?$/;

const getHostPortFromString = function (hostString, defaultPort) {
  let host = hostString;
  let port = defaultPort;

  let result = regex_hostport.exec(hostString);
  if (result != null) {
    host = result[1];
    if (result[2] != null) {
      port = result[3];
    }
  }

  return ([host, port]);
};

server.addListener('connect', function (req, socket, bodyhead) {
  let hostPort = getHostPortFromString(req.url, 443);
  let hostDomain = hostPort[0];
  let port = parseInt(hostPort[1]);
  console.log("Proxying HTTPS request for:", hostDomain, port);
  console.log(bodyhead.byteLength)

  let proxySocket = new net.Socket();
  proxySocket.connect(port, hostDomain, function () {
    proxySocket.write(bodyhead);
    socket.write("HTTP/" + req.httpVersion + " 200 Connection established\r\n\r\n");
  }
  );

  proxySocket.on('data', function (chunk) {
    socket.write(chunk);
  });

  proxySocket.on('end', function () {
    socket.end();
  });

  proxySocket.on('error', function () {
    socket.write("HTTP/" + req.httpVersion + " 500 Connection error\r\n\r\n");
    socket.end();
  });

  socket.on('data', function (chunk) {
    proxySocket.write(chunk);
  });

  socket.on('end', function () {
    proxySocket.end();
  });

  socket.on('error', function (err) {
    console.log(err)
    proxySocket.end();
  });

});
