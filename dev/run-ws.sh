#!/bin/bash

killbg() {
        for p in "${pids[@]}" ; do
                kill "$p";
        done
}

trap killbg EXIT
pids=()


for i in {1..15}
do
   node ../SocketIoStreamClient.js &
   pids+=($!)
done

node SocketClient.js
