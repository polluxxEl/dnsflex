process.title = 'ClientStreamApp';

const { Socket } = require('net');
const { WEB_SOCKET_SERVER_PORT } = require('../config');

const stageProxy = 'http://18.194.233.15:6100'
const localProxy = `http://localhost:${WEB_SOCKET_SERVER_PORT}`;
const localProxyNet = 'http://localhost:8124';

const socket = require('socket.io-client')(
  localProxy
  // stageProxy
);

socket.on('reconnecting', (attempt) => {
  console.log(`WebSocket client reconnect attempt ${attempt}`);
});

socket.on('connect', function () {
  console.log(`WebSocket client ${socket.id} connected`);
});

socket.on('disconnect', function () {
  console.log(`WebSocket client disconnected`);
});

socket.on('proxy-request', d => {
  let { port, hostDomain, bodyhead, httpVersion, unique, headers } = d;
  console.log(`\n\nGetting new Proxy request for: `, hostDomain, ':', port)
  console.log()
  let proxySocket = new Socket();
  console.log(`\nUNIQUE: `, unique)

  proxySocket.connect(port, hostDomain, function () {
    proxySocket.write(bodyhead);
    console.log(`CREATED NEW SOCKET TUNNEL FOR REQUEST`)
    socket.emit(unique + 'data', "HTTP/" + httpVersion + " 200 Connection established\r\n\r\n");
  });

  proxySocket.on('data', function (chunk) {
    socket.emit(unique + 'data', chunk);
  });

  proxySocket.on('end', function () {
    console.log(`Response is ended`)
    socket.emit(unique + 'end-response');
  });

  proxySocket.on('error', function (err) {
    console.log(err)
    socket.emit('data', "HTTP/" + httpVersion + " 500 Connection error\r\n\r\n");
    socket.emit(unique + 'error');
  });

  socket.on('data', function (chunk) {
    if (!proxySocket.destroyed)
      proxySocket.write(chunk);
  });

  socket.on(unique + 'end-request', function () {
    console.log(`Incoming request is End`)
    proxySocket.end();
  });

  socket.on('error', function (err) {
    console.log(err);
    proxySocket.end();
  });
});
