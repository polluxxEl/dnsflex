const http = require('http');
const fs = require('fs');
const path = require('path');
const { URL } = require('url');
const socketStream = require('socket.io-stream');
const SocketIoManager = require('./src/SocketIoManager');
const DBManager = require('./src/DBManager');
const ProxyManager = require('./src/ProxyManager');
const ProxyThrottler = require('./src/ProxyThrottler');
const util = require('./src/util');

const { PROXY_SERVER_PORT, WEB_SOCKET_SERVER_PORT, DB_CONNECTION, REST_API_PORT, USERNAMES_FILENAME } = require('./config');

process.title = 'DNS_Proxy_Node';

let knownUsernames = new Set();
const updateKnownUsernames = (usernames) => {
  knownUsernames.clear();
  usernames.split(/\r?\n/).forEach(username => knownUsernames.add(username));
  console.log(`Loaded ${knownUsernames.size} users from ${usernamesPath}`);
};

const usernamesPath = path.join(__dirname, USERNAMES_FILENAME);
updateKnownUsernames(fs.readFileSync(usernamesPath, { encoding: 'utf8' }));

fs.watch(usernamesPath, "utf8", (event, trigger) => {
  fs.promises.readFile(usernamesPath, { encoding: "utf8" }).then(updateKnownUsernames);
});

let restApiServer;

const dbManager = new DBManager(DB_CONNECTION);
const ioManager = new SocketIoManager(WEB_SOCKET_SERVER_PORT);
const proxyManager = new ProxyManager(dbManager, ioManager);
const proxyThrottler = new ProxyThrottler();

const processSocket = (req, socket, bodyhead) => {
  let hostPort = util.getHostPortFromString(req.url, 80);
  let hostDomain = hostPort[0];
  let port = parseInt(hostPort[1]);
  if (!req.headers['proxy-authorization']) {
    socket.write(
        `HTTP/${req.httpVersion} 407 Proxy Authentication Required\r\n` +
        `Proxy-Authenticate: Basic realm="Access to proxy"\r\n\r\n`);
    return socket.end();
  }
  let auth = req.headers['proxy-authorization'] || 'user ';
  let [sessionId, pass] = Buffer.from(auth.split(" ")[1], 'base64').toString().split(':');
  if (!knownUsernames.has(sessionId)) {
    return socket.end(`HTTP/${req.httpVersion} 403 Forbidden\r\n\r\n`);
  }

  const { bandwidth, threads } = util.parsePassword(pass);
  if (proxyThrottler.incConnections(sessionId, threads)) {
    socket.on('close', () => {
      proxyThrottler.decConnections(sessionId, threads);
    });
    proxyManager.getSocket(sessionId, pass)
        .then(appSocket => {
          console.log(`Request from ${socket.address().address}:${socket.address().port} to: ${req.url} via ${appSocket.id}`);
          const stream = socketStream.createStream({});
          socketStream(appSocket).emit('proxy-stream', stream, {
            port, host: hostDomain, head: bodyhead.toString(), httpVersion: req.httpVersion
          });
          const throttleGroup = proxyThrottler.getThrottleGroup(sessionId, bandwidth);
          socket.pipe(stream);
          if (throttleGroup) {
            stream.pipe(throttleGroup.throttle()).pipe(socket);
          } else {
            stream.pipe(socket);
          }

        })
        .catch(err => {
          console.log(err);
          socket.write("HTTP/" + req.httpVersion + ` 404 Not Found\r\n\r\n`);
          socket.end();
        });
  } else {
    console.log(`${sessionId} connections limit exceeded (${threads})`);
    socket.write(`HTTP/${req.httpVersion} 429 Too Many Requests\r\n\r\n`);
    return socket.end();
  }
};

const proxyServer = http.createServer();

ioManager.onSocketDisconnect(socket => {
  dbManager.deleteInactiveUserBySocketId(socket.id);
});

ioManager.onSocketConnect(socket => {
  const { address } = socket.handshake;
  const ip = addres443s.replace('::ffff:', '');
  util.getAppUserLocationByIp(ip)
    .then(location => dbManager.saveActiveAppUser({ socketId: socket.id, location, ip }));
});

ioManager.startSocketServer();

proxyServer.addListener('connect', processSocket);

proxyServer.addListener('error', (req, socket) => {
  console.log('ERROR', req, socket);
});

const respFormats = {
  '/list': { contentType: 'text/html', provider: util.getReportHtml },
  '/json': { contentType: 'application/json', provider: util.getReportJson },
  '/csv': {contentType: 'text/plain', provider: util.getReportCsv }
}

restApiServer = http.createServer(async (req, res) => {

  if (req.method === 'GET' && (req.url === '/list' || req.url === '/json' || req.url === '/csv')) {
    console.time(`\nREST_API ${req.method} - ${req.url}\n`)
    try {
      const clients = await dbManager.getAll();
      const result = await respFormats[req.url].provider(clients);
      const contentType = respFormats[req.url].contentType;
      res.setHeader('Content-Type', contentType);
      res.write(result);
      res.end();
      console.timeEnd(`\nREST_API ${req.method} - ${req.url}\n`)
    } catch (err) {
      console.timeEnd(`\nREST_API ${req.method} - ${req.url}\n`);
      console.log(`REST_API ERROR`);
      console.log(err);
      res.setHeader('Content-Type', 'text/html');
      res.write(err.toString());
      res.end();
    }
  } else {
    res.writeHead(404);
    res.end();
  }
}).listen(REST_API_PORT, () => console.log(`HTTP server with stats listening on`, REST_API_PORT));


proxyServer.listen(PROXY_SERVER_PORT, function () {
  console.log('Listening for HTTP Proxy connections on', PROXY_SERVER_PORT);
});

function exitHandler(code) {
  console.log(`Terminating app, exit code ${code}`)
  // wtf is 200?
  if (code == 200) {
    return
  }
  if (!dbManager) return

  return dbManager.deleteAllUsers()
    .then(ok => {
      process.exit(200)
    })
}

console.log(`Password schema is ${util.passwordSchema}`);

process.on('exit', exitHandler);
//catches ctrl+c event
process.on('SIGINT', exitHandler);
// catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR1', exitHandler);
process.on('SIGUSR2', exitHandler);

process.on('uncaughtException', (e) => {
  console.log(e);
});
