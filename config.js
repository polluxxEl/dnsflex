module.exports = {
  DEFAULT_MAX_CONNECTIONS: 10,
  DEFAULT_MAX_BANDWIDTH: 2048, // Mbps
  PROXY_SERVER_PORT: 5650, // To listen for HTTP Proxy connections
  IP2_KEY: 'O9WR2QV4ML',
  WEB_SOCKET_SERVER_PORT: 6100, // To listen for socket.io connections
  DB_CONNECTION: 'mongodb://localhost:9001/dnsflex',
  REST_API_PORT: 8080, // to show list of connected clients
  USERNAMES_FILENAME: 'usernames.txt', // allowed usernames, file automatically hot-reloaded
  IP_CACHE_TTL: 60 * 60 * 24, // IP to location requests cached to preserve credits
  IP_CACHE_SIZE: 10000, // max number of IPs to cache
  SOCKET_HISTORY_SIZE: 5, // how many sockets will be stored and skipped during rotation
};
